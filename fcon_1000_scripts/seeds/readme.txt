the file power.txt is the MNI coordinates from the orignal paper of Powers
et. al. (see supp. material). 

The file begining with UCLA_ is downloaded from this web
(http://umcd.humanconnectomeproject.org/umcd/default/update/1772). It seems the
study 'UCLA_Autism' use Power's ROIs in their work, as shown in the above link
(see Atlas:PowerNeuron_264). However, the order of the ROIs is different with
the power.txt file. To use the ROI names and abbreviate names, you should use
all files begining with UCLA, insteadl of the power.txt. 

