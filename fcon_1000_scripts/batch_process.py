import os
import subprocess
import fnmatch
import sys
import random
import shutil

def batch_process(site_dir, timepoints, script_dir=None):
    """
    fcon batch_process script, and save to session_dir
    """
    # If no script directroy given, default to current directory
    if not script_dir:
        script_dir = os.path.dirname(os.path.abspath(__file__))

    anat_name = 'mprage'
    func_name = 'rest'
    standard_brain = "%s/templates/MNI152_T1_3mm_brain_mask.nii.gz" % script_dir
    tissue_prior = "%s/tissuepriors/3mm" % script_dir
    nuisance = "%s/templates/nuisance.fsf" % script_dir

    all_sub_folders = os.listdir(site_dir)
    all_sub_folders.sort()

    proc_set = set()
    for sub_name in all_sub_folders:
        data_dir = os.path.join(site_dir, sub_name, 'session_1')
        command = "%s/1_anatpreproc.sh %s %s %s" % (script_dir,
                                            sub_name,
                                            data_dir,
                                            anat_name)
        proc_set.add( subprocess.Popen(command, shell = True) )
        print("Beginning: %s" % command)

    for p in proc_set:
        if p.poll() is None:
            p.wait()

    proc_set.clear()
    for sub_name in all_sub_folders:
        data_dir = os.path.join(site_dir, sub_name, 'session_1')
        command = "%s/2_funcpreproc.sh %s %s %s 0 %s 2" % (script_dir,
                                                   sub_name,
                                                   data_dir,
                                                   func_name,
                                                   str(int(timepoints)-1))
        proc_set.add( subprocess.Popen(command, shell = True) )
        print("Beginning: %s" % command)

    for p in proc_set:
        if p.poll() is None:
            p.wait()

    proc_set.clear()
    for sub_name in all_sub_folders:
        data_dir = os.path.join(site_dir, sub_name, 'session_1')
        command = "%s/3_registration.sh %s %s %s %s" % (script_dir,
                                                sub_name,
                                                data_dir,
                                                anat_name,
                                                standard_brain)
        proc_set.add( subprocess.Popen(command, shell = True) )
        print("Beginning: %s" % command)

    for p in proc_set:
        if p.poll() is None:
            p.wait()

    proc_set.clear()
    for sub_name in all_sub_folders:
        data_dir = os.path.join(site_dir, sub_name, 'session_1')
        command = "%s/4_segment.sh %s %s %s %s %s" % (script_dir,
                                              sub_name,
                                              data_dir,
                                              anat_name,
                                              func_name,
                                              tissue_prior)
        proc_set.add( subprocess.Popen(command, shell = True) )
        print("Beginning: %s" % command)

    for p in proc_set:
        if p.poll() is None:
            p.wait()


    proc_set.clear()
    for sub_name in all_sub_folders:
        data_dir = os.path.join(site_dir, sub_name, 'session_1')
        command = "%s/5_nuisance.sh %s %s %s 2 %s %s" % (script_dir,
                                                 sub_name,
                                                 data_dir,
                                                 func_name,
                                                 timepoints,
                                                 nuisance)
        proc_set.add( subprocess.Popen(command, shell = True) )

    for p in proc_set:
        if p.poll() is None:
            p.wait()

if __name__ == '__main__':
    batch_process(sys.argv[1], sys.argv[2])
