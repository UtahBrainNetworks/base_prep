# Utah f-MRI processing pipeline #

This is a guide for processing MRI images using [fcon_1000](http://fcon_1000.projects.nitrc.org/) pre/post processing scripts and an in-house method for seed-based correlation analysis using [Power's ROIs](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3222858/). The fcon_1000 project has been superseded by [CPAC](http://fcp-indi.github.io/docs/user/index.html). So future work may want to start using the new interface.

To my knowledge, this only works on *nix systems, so the instructions will assume this environment. The commands assume a Debian based OS using 'apt-get' package manager.

## Installation ##

First, clone this repository. The scripts used in preprocessing require [FSL](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FSL), [AFNI](https://afni.nimh.nih.gov/), and [ANTS](http://stnava.github.io/ANTs/) functions to be on the system path. The seed-based correlation analysis must be compiled from source.

### fcon scripts ###
Follow the instructions on the following pages to install the necessary packages:

* [ANTS](https://brianavants.wordpress.com/2012/04/13/updated-ants-compile-instructions-april-12-2012/)
* [AFNI](https://afni.nimh.nih.gov/pub/dist/doc/htmldoc/background_install/install_instructs/index.html)
* [FSL](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FslInstallation)

### compilation ###

1. Compilation requires `cmake`, to install:
    `sudo apt-get install cmake`
2. Download and install [Boost](http://www.boost.org/doc/libs/1_61_0/more/getting_started/unix-variants.html) and [ITK](https://itk.org/Wiki/ITK_Configuring_and_Building_for_Ubuntu_Linux) packages. Their installation must either be on the system path or in a known location for use in cmake.
3. Edit the CMakeLists.txt file in the `corr` directory and edit the `Boost_INCLUDE_DIR` and `ITK_DIR` variables to their location on your system. If the packages are already on the system path, this step can be skipped and the variables deleted.
4. Go into the `corr` directory like `cd /path/to/corr/` and compile with `cmake ./ && make`

## Use ##

### fcon_1000 scripts ###

There are 3 scripts used for processing:

* 0_preprocess.sh
    * Runs scripts 1-5 on a single subject
    * Local variables must be edited in script
* batch_process.sh
    * Runs script 0 and 6-8 on a list of subjects
    * Local variables must be edited in script
* batch_process.py
    * Only requires site_name directory and timepoints as input
    * Runs scripts 1-5 on subjects in given data directory

The easiest to use is the batch_process.py script. Simply run it like:
```python
python batchprocess.py '/path/to/site/directory' 'timepoints'
```

The other scripts require editing of variables in the script specific to the use.

### seed-based correlation
Go into the `bin` directory and run `./parcelize -h` for information on running. The Power ROI coordinates are in the `fcon_1000_scripts/seeds/` directory.

## Script Description ##

This first 5 scripts are considered 'preprocessing' with the last three which can be considered as 'postprocessing'.

The processing scripts and their steps are as follows:

* 1_anatpreproc.sh
    1. Deoblique anatomical image
    2. Reorient to fsl-friendly space
    3. Skull strip
* 2_funcpreproc.sh
    1. Drops specified starting TR's (default in batch processing is 0)
    2. Deoblique functional image
    3. Reorient to fsl-friendly space
    4. Motion correct to average of timeseries
    5. Skull strip
    6. Get eigth image for use in registration
    7. Spatial smoothing
    8. Grandmean scaling
    9. Temporal filtering
    10. Detrending
    11. Creating mask
* 3_registration.sh
    1. Create a registration directory and copy required images to directory
    2. Go into directory
    3. Convert functional -> T1
    4. Convert T1 -> standard
    5. Convert functional -> standard
* 4_segment.sh
    1. Create segment directory
    2. Go into directory
    3. Segment the brain
    4. Copy required images
    5. Register csf to native space
    6. Smooth image to match smoothing on functional
    7. Register to standard
    8. Find overlap with prior
    9. Revert back to functional space
    10. Threshold and binarize probability map of csf
    11. Mask again by the subject's functional
    12. Register wm to native space
    13. Smooth image to match smoothing on functional
    14. Register to standard
* 5_nuisance.sh (Could also be considered postprocessing)
    1. Make nuisance directory
    2. Separate motion parameters into separate files
    3. Signal extraction for global
    4. Signal extraction for csf
    5. Signal extraction for wm
    6. Generate mat file
    7. Get residuals
    8. Demean residuals and add 100 (Steps 8 & 9 are used for future use of FEAT)
    9. Resampling residuals to MNI space
* 6_singlesubjectRSFC.sh
    1. Extract timeseries
    2. Compute voxel-wise correlation with seed timeseries
    3. Z-transform correlations
    4. Register Z-transformed correlations to standard space
* 7_singlesubjectALFF.sh
    1. Preprocess image
    2. Calculate ALFF and fALFF
* 8_singlesubjectDR.sh
    1. Computes non-normalized dual regression